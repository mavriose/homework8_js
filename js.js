let btn = document.getElementById('btn');
let div1 = document.getElementById('divError');
btn.addEventListener('blur', addDiv);

function addDiv() {
    let check = document.getElementsByClassName('active');

    if (check.length) check[0].remove();
    if (btn.value > 0) {
        let div2 = document.getElementsByClassName('box');
        let span = document.createElement('span');
        span.classList.add('active');
        div2[0].appendChild(span);
        btn.style.color = 'green';
        span.innerHTML = `Текущая цена:<span class="priceValue">${btn.value}</span><a href="#" class="close" id="close">x</a>`;
        div1.style.display = 'none'
    } else if (btn.value <= 0) {
        div1.style.display = 'block';
        btn.style.color = ''
    }
    const closeModal = document.querySelectorAll('.close');
    for (let i = 0; i < closeModal.length; i++) {
        closeModal[i].addEventListener('click', close);
    }

}

function close() {
    const span = this.closest('.active');
    span.remove();
    let btn = document.getElementById('btn');

    btn.value = '';
}